# -*- coding: utf-8 -*-
import numpy as np
from math import *
from random import *
import matplotlib.pyplot as plt

class Bisection:

    def __init__(self, settings):
        self.intervals = []
        self.approx = []
        self.a = settings.a
        self.b = settings.b
        self.solution = None
        self.epsilon = settings.epsilon
        self.int_s = randrange(self.a, self.b-2)
        self.int_e = randrange(self.int_s, self.b)
        while self.calc_function(self.int_s) * self.calc_function(self.int_e) > 0:
            self.int_e = randrange(self.a, self.b)
            self.int_s = randrange(self.a,self.int_e)
        self.m_z = self.bisect()
        print self.m_z

    def calc_function(self,x_0):
        e = 2.71828182846
        return e**x_0-1.5-atan(x_0)

    def bisect(self):
        x_0 = None
        c = 0
        while fabs(self.calc_function(self.int_s) + self.calc_function(self.int_e)) > self.epsilon:
            print "przedział : (%s,%s)"%(round(self.int_s,5),round(self.int_e,5))
            c+=1
            x_0 = float(self.int_s+self.int_e)/2
            f_0 = self.calc_function(x_0)
            if fabs(f_0) < self.epsilon:
                break
            elif f_0*self.calc_function(self.int_s) <0:
                self.int_e = x_0
            else:
                self.int_s = x_0
            print "Przybliżenie %s:"%c, x_0

            #dodanie do punktu do listy przyblizen
            self.add_approx(x_0)

        self.solution = x_0
        return x_0


    def add_approx(self,approx):
        self.approx.append(approx)

    def plot(self):
        fig = plt.figure()

        #wykres funkcji
        functionPlot = fig.add_subplot(111)
        x = np.arange(-20, max(self.approx)+1, 0.01)
        y = map(self.calc_function, x)
        functionPlot.plot(x, y, 'r-')
        functionPlot.grid(True)
        functionPlot.set_ylabel('Y')
        functionPlot.set_xlabel('X')
        functionPlot.set_title('Bisection #'+str(len(self.approx)))

        #wykres przybliżeń
        aproxPlot = fig.add_subplot(111)
        x = self.approx
        y = map(self.calc_function, x)
        aproxPlot.plot(x, y, 'b.')

        #wykres zakresów X
        rangePlotX = fig.add_subplot(111)
        rangePlotX.hlines(0, min(self.approx), max(self.approx), colors='k', linestyles='solid', label='Zakresy')

        #wykres zakresów Y
        rangePlotY = fig.add_subplot(111)
        rangePlotY.vlines(self.solution, min(y), max(y), colors='k', linestyles='solid', label='Zakresy')

        #pokaz wykresy
        plt.show()


if __name__ == "__main__":
    bs = Bisection()
    bs.plot()







