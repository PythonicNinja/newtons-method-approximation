# -*- coding: utf-8 -*-
__author__ = 'wojtek'

import numpy as np
import multiprocessing

import newtons
import bisection

class Settings:

    def __init__(self, epsilon=0.0000000001, a=-20, b=0):
        self.epsilon = epsilon
        self.a = a
        self.b = b

    def F(self,x):
        return np.e**x - 1.5 - np.arctan(x)

    def Df(self,x):
        return np.e**x - 1.0/(x**2+1)


def bisect(process_name, settings_obj):
    print process_name

    bs = bisection.Bisection(settings_obj)
    bs.plot()


def newton(process_name, settings_obj):
    print process_name

    h = newtons.Newton(settings_obj)
    h.plot()

if __name__ == "__main__":

    settings_obj = Settings()

    p1 = multiprocessing.Process(target=bisect, args=('Process bisekcji 1', settings_obj))
    p2 = multiprocessing.Process(target=newton, args=('Process newtona 2', settings_obj))

    p1.start()
    p2.start()

    p1.join()
    p2.join()