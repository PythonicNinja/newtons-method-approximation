# -*- coding: utf-8 -*-
import numpy as np
import random
import matplotlib.pyplot as plt
__author__ = 'vojtek.nowak@gmail.com'


class Newton:

    def __init__(self, settings):
        self.settings = settings
        self.guess_old = random.randrange(self.settings.a, self.settings.b)
        self.approx = []

        self.guess = self.guess_old - self.settings.F(self.guess_old)/self.settings.Df(self.guess_old)

        print 'Zgadywana wartosc 1: ' + str(self.guess_old)
        print 'Obliczona wartosc 2: ' + str(self.guess)
        self.approx.append(self.guess_old)
        self.approx.append(self.guess)

        i = 0
        while(abs(self.guess_old - self.guess)>self.settings.epsilon):
            self.guess_old = self.guess

            self.guess = self.guess_old - self.settings.F(self.guess_old)/self.settings.Df(self.guess_old)
            print 'Przyblizenie ' + str(i) + ': ' + str(self.guess)
            self.approx.append(self.guess)
            i += 1
        print(self.guess)


    def plot(self):
        fig = plt.figure()

        #wykres funkcji
        functionPlot = fig.add_subplot(111)
        x = np.arange(-20, max(self.approx)+1, 0.01)
        y = map(self.settings.F, x)
        functionPlot.plot(x, y, 'r-')
        functionPlot.grid(True)
        functionPlot.set_ylabel('Y')
        functionPlot.set_xlabel('X')
        functionPlot.set_title('Newtons Method #'+str(len(self.approx)))

        #wykres przybliżeń
        aproxPlot = fig.add_subplot(111)
        x = self.approx
        y = map(self.settings.F, x)
        aproxPlot.plot(x, y, 'b.')

        #wykres zakresów X
        rangePlotX = fig.add_subplot(111)
        rangePlotX.hlines(0, min(self.approx), max(self.approx), colors='k', linestyles='solid', label='Zakresy')

        #wykres zakresów Y
        rangePlotY = fig.add_subplot(111)
        rangePlotY.vlines(self.approx[-1], min(y), max(y), colors='k', linestyles='solid', label='Zakresy')

        #pokaz wykresy
        plt.show()

if __name__ == "__main__":
    h = Newton()
    h.plot()


